# tp2_serverless_datalake_with_api

The images display below show the project was succesful

Go to pipeline «add EC2 CI» to see the pipepline is successfull before we did the terraform destroy command.

## 3.6.2 QUERY MSCK REPAIR

![image](/images_readme/TP2_MSCK_REPAIR.png "TP2_MSCK_REPAIR")

## 3.6.2 SELECT QUERY through aws athena


![image](/images_readme/TP2_SELECT_ATHENA.png "TP2_SELECT_ATHENA")

## 3.6.2 other exemple SELECT QUERY through aws athena


![image](/images_readme/TP2_SELECT_ATHENA_2.png "TP2_SELECT_ATHENA_2")

## 3.8 Data visualization on EC2 instance


![image](/images_readme/TP2_AFFICHAGE_EC2_OFFERA.png "TP2_AFFICHAGE_EC2_OFFERA")
